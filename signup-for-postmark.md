# Signup and Configure a Postmark Account

In this exercise, you will signup for a Postmark account, create a new server and create an template for order confirmation email. You will also use `curl` to send an API request to test the new server and the email template in Postmark.

* Head to [Postmarkapp](https://postmarkapp.com) and signup for a free trial.
* Create a `Sender Signature` that will be used to send emails.
> The email used in `Sender Signature` is the only email that you can send emails from.

## Create a server and email template

* Under `Server > My First Server > API Tokens`, note the Server API token from the box.
* Click on `Templates` tab and click `Add Template` to add a `Code your own` template.
* Choose `Don’t use a layout` and click `continue`.
* Copy and paste the following code to replace everything in the template. _ Make sure you are in `Edit` mode.

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <style type="text/css"></style>
  </head>
  <body>
    <p>
      Thank you for your order {{customer_name}}. The total for your order was
      {{order_total}}
    </p>
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Quantity</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {{#each order_items}}
        <tr>
          <td>{{name}}</td>
          <td>{{quantity}} x {{meta.display_price.with_tax.unit.formatted}}</td>
          <td>{{meta.display_price.with_tax.value.formatted}}</td>
        </tr>
        {{/each}}
      </tbody>
    </table>
  </body>
</html>
```

Once you’ve saved the template, you will need to make a note the ID of the template:

![Template ID](images/postmark-template.png)

## Test the server

* Under `Server > My First Server > Default Transactional Stream > Setup Instructions`, click curl tab and copy the curl command.
* Paste the command into your terminal and run to send a test email.
* Confirm the receipt of the test email.

>While your account is in test mode, you can only send send emails to your domain and the domains that you have verified.

>If the test email is not delivered, check the details under `Server > My First Server > Default Transactional Stream > Activity` tab.

[Next: Create a Serverless Function](./serverless-function.md)