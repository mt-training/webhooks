# Create Integration

In this exercise, you will create a new integration using API requests in Postman. This integration will trigger the webhook on `order.paid` event. The order details will be sent to the endpoint URL that will process the payload and use Postmark to send the order confirmation email.

## Placing an order

You must complete the following steps using API requests:

* Create a new integration
* Create a product
* Add the product to your cart
* Create a customer
* Checkout (place an order)
* Authorize payment
* Capture payment

### Create a new integration

* In Postman, open the `Create an integration` request under `integrations` folder.
* Replace the contents in the `Body` section with:

```json
{
    "data": {
        "type": "integration",
        "integration_type": "webhook",
        "enabled": true,
        "name": "Serverless Function",
        "description": "Pass data to function when order is paid/captured",
        "observes": [
            "order.fulfilled",
            "order.paid"
        ],
        "configuration": {
            "url": "NGROK HTTP URL",
            "secret_key": "AS CONFIGURED in .env"
        }
    }
}
```

> Don't forget to update the `url` and `secret_key`
* Select `Send`.

### Create a product

* In Postman, open the `Create a product` request under `products_legacy` folder.
* Make sure you are using a unique `slug` and `sku` and the `currency` is updated to the one configured for your store.
* Select `Send`.

> The ID of the product is saved in `productID` environment variable, so it can be used in other requests.

### Add the product to your cart

* To add the item you created in the last step to your cart, send a request using the `Add product to cart` under `carts -> cart_items` folder.

### Create a customer

* In Postman, open the `Create a customer` request under `customers` folder.
* Fill up the details in the body of the request.
* Select `Send`.

> The ID of the customer is saved in `customerID` environment variable, so it can be used in other requests.

### Checkout (place an order)

* In Postman, open the `Checkout as customer` request under `checkout` folder.
* Fill up the details in the `billing_address` and `shipping_address` in the body of the request.
* Select `Send`.

> The ID of the order is saved in `orderID` environment variable, so it can be used in other requests.

### Authorize payment

* Now, authorize payment for your order by sending an `Authorize` request under `payments-> manual` folder.

> Make sure you have `Manual` payment gateway enabled. To enable it from the [dashboard](https://dashboard.elasticpath.com/app/settings/gateways/manual), go to `Settings > Payment Gateways` and enable `Manual` payment gateway from the list.
> ![Payment Gateway](images/manual-gateway.png)

### Capture payment

* To capture payment for your authorized payment, send a request using the `Capture a transaction` under `transactions` folder.

### Verify

That’s all! You have successfully placed an order and that should have generated the event to trigger the webhook.

* Confirm the receipt of the order confirmation email.

## Troubleshoot

If the email is not delivered, complete these troubleshooting steps:

1. Head back to [Postmark](https://postmarkapp.com) to check the details under `Server > My First Server > Default Transactional Stream > Activity` tab.
2. Get the integration logs. In Postman, send a request using `Get Logs for an Integration` under `Integrations` folder.
3. Open `localhost:4040` in a browser to open `ngrok web inspection UI`. It allows you to see a log of all your requests. 
>You can use the `Replay` button at the top-right corner of any request to replay it.
4. Check for the error message, if any, in the server terminal.