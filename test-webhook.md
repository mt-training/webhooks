# Test Webhook

In this exercise, you will create a webhook integration that observes `customer.created` and `customer.deleted` events. You will use a temporary endpoint URL from Webhook.site to analyze the request JSON.

## Generate a random URL

Visit [Webhook.site](https://webhook.site) and note the randomly generated URL. Leave the browser tab open as you will revisit this later to check on the request.

![Webhook Site](images/webhook.site.png)

## Create a new integration

You must now create a new integration using the URL above. Head to the [Dashboard](https://euwest.cm.elasticpath.com/settings/webhooks), login and go to `Settings > Webhooks` and click `New integration`.

Enter a name and description for your Integration. We recommend you prefix the name with `TRAINING:`.

Next, enter the `URL` and leave the `Secret Key` blank.

![URL and Secret Key](images/webhook_config.png)

Now finally you’ll want to configure when this webhook will be invoked, check the `Created` and `Deleted` box for `Customer` and `Save`.

![Observes selection](images/webhook_customer.png)

## Test integration

Head to `Settings > Customer` and click on `New Customer`.

Enter the details and click `Save` to create a new customer.

## Verify

You have successfully created a new customer and that should have generated an event to trigger the webhook.

* Head back to [Webhook.site](http://webhook.site) and confirm the receipt of the event.

[Next: Signup for Postmark](./signup-for-postmark.md)