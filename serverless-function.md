# Create a Serverless Function

In this exercise, you will create a serverless function in Node.js to process the event payload and use Postmark to send the order confirmation email. You will be:

* Cloning the repository and install dependencies
* Running a local dev server

## Clone the repository

Clone the repository that has a node module to parse the event message and use Postmark API to send a templated email:

```bash
$ git clone https://github.com/moltin/examples.git
```

Install dependencies:

```bash
$ cd examples/webhooks/order-confirmation-email

$ yarn
```

>This is a sample repository created in NodeJS. You can create a similar one using the programming language of your choice. You can also use AWS Lambda functions or Azure Functions or any other serverless functions as the URL.

## Run a local dev server

You will want to create a `.env` file inside the directory `/order-confirmation-email` containing all the keys for the below:

```shell
POSTMARK_API_KEY=
MOLTIN_WEBHOOK_SECRET=
POSTMARK_FROM_ADDRESS=
POSTMARK_CONFIRMATION_TEMPLATE_ID=
```

`MOLTIN_WEBHOOK_SECRET` can be anything you want. You will use this when creating a new webhook integration in the next exercise.

* Start the development server:

```bash
yarn dev
```

The server will typically start on port `3000`, if not, make a note for the next step.

* In a **new** terminal window, start ngrok:

```bash
ngrok http 3000
```

This will expose port `3000` to the outside world. Make a note of the `http` URL, _not the localhost one_, that you will use as the endpoint URL in your next integration.

### Inspecting your traffic in ngrok

Ngrok provides a real-time web UI where you can introspect all of the HTTP traffic running over your tunnels. After you've started ngrok, just open `http://localhost:4040` in a web browser to inspect request details.

Ngrok allows you to replay any request. Click the `Replay` button at the top-right corner of any request to replay it.

[Next: Create an Integration (Webhooks)](./integration.md)